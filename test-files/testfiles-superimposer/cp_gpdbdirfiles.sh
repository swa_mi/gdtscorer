#!/bin/bash
#PBS -N cp_files
#PBS -J 1-882:1
#PBS -e /storage/swastik/erroutfiles/cp_^array_index^.err
#PBS -o /storage/swastik/erroutfiles/cp_^array_index^.out

cd $PBS_O_WORKDIR
#hostname

read -a array <<< $(cat /home/swastik/bin/scoreanypdb/gdtscorer/test-files/testfiles-superimposer/r3_r1_r1_r1_r2_r2_r2_r8.gpdb.list)
filename=${array[`expr $PBS_ARRAY_INDEX - 1`]}
echo $filename

#if [ -d "/tmp/workingswastik/" ];then

    #rm -r /tmp/workingswastik
#hostname
#fi

#if [ -d "/tmp/swastik/" ];then

    #rm -r /tmp/swastik
#hostname

#fi
cp /storage/swastik/db/gpdb_db_2018dec15/$filename".gpdb" /home/swastik/bin/scoreanypdb/gdtscorer/test-files/testfiles-superimposer/gpdbdir/
#sleep 20
