/*
 * Superimpose.cpp
 *
 * Created on: 2018-10-10
 *      Author: Swastik
 */


#ifndef SUPERIMPOSE_H_
#define SUPERIMPOSE_H_

//#include <string>
#include <boost/algorithm/string.hpp>

float superimpose_stars(
    std::vector< std::vector<double> > &store_str1repatom, // this will store coordinates for query
    std::vector< std::vector<double> > &store_str2repatom,
    float &penalty,
    float &sup_cutoff
    )
;

// float function that calculates the GDTTS score of top 100 matches given in input as a vector
float score_calculator(
    std::vector<float> rmsdvalues,
    float last_cutoff
    )
;

void superimpose_stars_2(
    std::vector<std::vector<std::string>> &allpermutvecs,
    std::string &target_star,
    int &star_size,
    std::map<std::string, std::vector<double> > q_map_strcoordinates, // map of pairs of atomdetails and coordinates
    float &penalty,
    float &sup_cutoff,
    std::vector< std::pair<float, std::vector<std::string> > > &allsuperimps

    )
;
#endif
