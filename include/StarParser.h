/*
 * starParser.h
 *
 * Created on: 23-Jul-2018
 * Author: Swastik
 */

#include <string>
#include "vector"
#include "bits/stdc++.h"

#ifndef STARPARSER_H_
#define STARPARSER_H_


void Scan_ATOM_for_coord(const std::vector<std::string> & vec_atomstatements,          // vector with ATOM statements for members of star
    std::vector<std::pair<std::string, std::vector<double>> > &vecmap_strcoordinates,  //  atomdetails: coordinates pairs
    std::map<std::string, std::vector<double> > &map_str1coordinates                          //the map of --do--
    )

;
void Scan_star_for_stars(const std::vector<std::string> starvector,                // the filename of the .star file to be read
    std::vector<std::string>& vec_atomstatements,             // map of line number to atom statements in gpdb file, for star only
    std::map<int, std::string> &map_atomstatements,          //  map of line number to atom statements in gpdb file, for whole file
    std::ifstream& gpdbif
    )
;


#endif /* starPARSER_H_ */
