/*
 * Inputprocessor.cpp
 *
 * Created on: 2018-10-26
 *      Author: Swastik
 */

#ifndef INPUTPROCESSOR_H_
#define INPUTPROCESSOR_H_


#include <boost/algorithm/string.hpp>

void query_processor(std::string &query_star,       //query star of the form 101\t102\t103...
    int &star_size,
    std::map<std::string, std::vector<double> > &q_map_strcoordinates,               //   the map of pairs of 
                                                                         //     atom-details:coordinates
    std::vector<std::vector<std::string>> &allpermutvecs,
    std::string &query_composition                          // query star compo in terms of cg_names
    )
;
void target_processor(
    std::string &query_composition,              // query composition, underscore delimited
    std::string &cliqs_dir,                     // target_dir with the .cliqs files
    int &sample_size,                            
    std::vector<std::string> &targetstars,
    std::string &target_dir,
    std::string &query_gpdbid,
    std::string &reject_pdbid
    )
;

#endif
