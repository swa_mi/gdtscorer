SHELL = /bin/bash
SHELLFLAGS="-O extglob -c"

# Create Variables to Each Directory of the Project
SRCDIR = src                                                           
LIBDIR = lib
DOCDIR = doc
HDRDIR = include
BIN = bin

#CFLAGS = -g -Wall -O3
CXX = g++
CXXFLAGS = -std=c++11 -lboost_program_options -I ${HDRDIR}

#Get Only the Internal Structure of SRCDIR Recursively
STRUCTURE := $(shell find $(SRCDIR) -type d)

#Get All Files inside the STRUCTURE Variable
CODEFILES := $(addsuffix /*,$(STRUCTURE))
CODEFILES := $(wildcard $(CODEFILES))

#Filter Out Only Specific Files
SRCFILES := $(filter %.cpp,$(CODEFILES))
HDRFILES := $(filter %.h,$(CODEFILES))
OBJFILES := $(patsubst $$SRCDIR,$$SRCDIR,$(SRCFILES:%.cpp=%.o))

# Filter Out Function main for Libraries
LIBDEPS = $(filter-out Main.o,$(OBJFILES))
#$(info $$var is [${LIBDEPS}])

#Now it is Time to create the Rules
compile: $(OBJFILES)

$LIBDEPS: $(addprefix $(SRCDIR)/,%.cpp %.h)
	$(CXX) $(CXXFLAG) -c $< -o $(SRCDIR)/$@ 

all: $(LIBDEPS)
	$(CXX) $(CXXFLAGS) -o ${BIN}/starscorer.bin $(LIBDEPS)

.PHONY : clean
clean :
	rm $(OBJFILES)
