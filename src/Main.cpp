
/* Copyright 2018, Swastik Mishra
 * swastikm@protonmail.com
 * 
 * Main.cpp
 *
 * Created on: 2017-10-01
 * Author: Swastik
 */

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include "jacobi_eigenvalue.h"
#include "AlignInterface.h"
#include "vector_operation.h"
#include "StarParser.h"
#include "Superimpose.h"
#include "Inputprocessor.h"

//using namespace std;
//using namespace boost;
// won't use namespace from now on

inline bool file_exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }   
}

int main(int argc, char** argv) {


    std::vector<vector<double> > store_str1repatom; // this will store coordinates for query
    std::vector<vector<double> > store_str2repatom; // this will store coordinates for target inside the for loop (see below)
    store_str1repatom.clear();
    store_str2repatom.clear();


    vector_operation vo;
    //AlignInterface ai;
    

    int star_size ;
    float sup_cutoff ;
    float penalty ;
    int sampling_size ;
    float gdtrmsdcutoff ;
    std::string query_dir;
    std::string target_dir;
    std::string cliqs_dir;
    std::string reject_pdbid;

    try
    {
        namespace po = boost::program_options;

        // Declare the supported options.
        po::options_description desc("Allowed options");
        desc.add_options()
        ("help,h", "produce help message")
        ("input-starfile,i", po::value<string>(), "path to .star file")
        ("query-dir,q", po::value<string>(&query_dir)->required(), "Path to dir where query should be extracted from")
        ("target-dir,t", po::value<string>(&target_dir)->required(), "Path to dir where targets should be extracted from")
        ("size,n", po::value<int>(&star_size)->default_value(7), "Set size of star")
        ("rmsd,r", po::value<float>(&sup_cutoff)->default_value(2.0), "Set rmsd cutoff for succesful superimposition/match")
        ("penalty,p", po::value<float>(&penalty)->default_value(10.0), "Set penalty for non-superimposition")
        ("sampling,s", po::value<int>(&sampling_size)->default_value(100), "Sampling size")
        ("cliqs-dir,c", po::value<string>(&cliqs_dir)->required(), "Cliqs directory where .cliqs file are present")
        ("gdt-rmsd-cutoff,g", po::value<float>(&gdtrmsdcutoff)->default_value(2.0), "Set rmsd cutoff till which gdt fractions are calculated, starting from 1.0")
        ("rejectID,j", po::value<string>(&reject_pdbid)->required(), "Comma separated (no spaces) PDB-IDs to be excluded for homologous structures; e.g. 1onc,1ang,1mdc")

        ;

        po::variables_map vm; 
        try 
        { 
          po::store(po::parse_command_line(argc, argv, desc),  
                    vm); // can throw exception
     
            // --help option 
            if ( vm.count("help")  )
            {
                std::cout << "Superimposition code for stars" << std::endl
                << desc << std::endl;
                return 0;
            }

            po::notify(vm); // throws on error, so do after help in case
                                    // there are any problems
        }
        catch (po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return 1;
        }


        float rmsd;
        float bm_rmsd;
        //double rmsd = penalty;
        //double rmsd2 = penalty;
        //float so=0.0;
        //float so2=0.0;

        std::string line;                                // string to store each line while reading stdin or .star file
        std::vector<string> vec_atomstatements;     // vector to store atom statements from gpdb file
        std::string query_star;;                    //first line of .star file 
        std::vector<std::string> targetstars;

        std::string query_gpdbid;
        int linecount = 0;


        if( vm.count("input-starfile"))
        {
            ifstream inputstarfile(vm["input-starfile"].as<std::string>());
            if( inputstarfile.is_open() )
            {
                while( getline(inputstarfile, line) )
                {
                    //cout << line << endl;
                    if (linecount==0)
                    {
                        query_star = query_dir+"/"+line;
                    } // end if block checking for first line or not
                    //else
                    //{
                        //targetstars.push_back(target_dir+"/"+line);
                    //} // end else block 

                    linecount = linecount+1;
                } // end while block looping over lines in .star filestream object
            } // end if block checking if file is open
        } // end if block checking if input-starfile was passed as argument
        else 
        { // else read from stdin - actually means if stdin is being redirected
            if ( isatty (0) )
            { // there was no stdin redirection
                throw std::invalid_argument("ERROR: STDIN unused and input-starfile argument empty.\nSee --help for usage.\nExiting.\n\n");
            }
            else
            {
                while( getline(cin, line) ){
                    //cout << line << endl;
                    if (line.empty()){
                        throw std::invalid_argument("ERROR: STDIN is empty. Exiting.\n\n");
                    }
                    else{ // stdin isn't empty
                        //cout << "line is" << line << endl;
                        if (linecount==0){
                            query_gpdbid = line;
                            query_star = query_dir+"/"+line;
                        }
                        else{
                            targetstars.push_back(target_dir+"/"+line);
                        }
                    }
                    linecount = linecount+1;
                } // end looping over filestream object
            } // end checking if stdin was redirected
        } // end reading .star file
        //std::cout << targetstars.size() << " is the number of target stars found " << endl;
        /* At this point query_star is the first line of .star file
         and targetstars is vector<string> with the rest of the lines
        */


        // QUERY star PROCESSING starts here
        // process query and return two things:
        std::map<string, vector<double> > q_map_strcoordinates;               //   the map of pairs of 
                                                                             //     atom-details:coordinates
        std::vector<std::vector<std::string>> allpermutvecs;        // vector with permutation vectors of the 
        std::string query_composition;                        // composition of the query star in terms of cg_names
        // use query star processor function
        query_processor(query_star,star_size, q_map_strcoordinates, allpermutvecs, query_composition);
        //std::cout << query_composition << std::endl;

        /**allpermutvecs now has vectors such that
        each vector is a permutation of the atoms in query star
        however only points except the first point(central point) are permuted, so # of permutations is greatly reduced
        permute only last n-1 atoms/chm-groups
        */

        std::vector< pair<float, std::vector<std::string> > > allsuperimps;
        //all superimpositions to be stored as a pair of rmsd and vector of strings of atom-details
        /** loop over each string in targetstars vector
            for each string in the vector, find coordinates and superimpose on q_vecmap_strcoordinates
            find rmsd and append rmsd,details to a vector
        */

        /*
        Find target stars by using:
            - query composition
            - target stars directory
            - N = sampling size for target stars out of all the stars of the query composition
        */
        //std::cout << "debug after this" << std::endl;

        target_processor(query_composition, cliqs_dir, sampling_size, targetstars, target_dir, query_gpdbid, reject_pdbid);

        /**if targetstars vector is empty, it means that no star with same composition 
        was found in the database and hence, we should abort, giving this match a penalty for non-matching
        */
        if ( !targetstars.empty() )
        {

            std::cout << "Num of targets: " << targetstars.size() << std::endl;
            //vo.print_vector(targetstars);
            //std::string::iterator target_star;
            //std::cout << "Starting superimposition..." ;
            for (auto target_star=targetstars.begin(); target_star!=targetstars.end() ;target_star++)
            {
                superimpose_stars_2(allpermutvecs, *target_star, star_size, 
                                    q_map_strcoordinates, penalty, sup_cutoff,
                                    allsuperimps);
            }
            //display allsuperimps which is a vector of pairs
            //such that the key in each pair is the rmsd of the superimp and the value
            //is SO+"#"+atomids
            /*std::vector<pair<double, std::vector<string> >>::iterator superimpiter;
            for (superimpiter=allsuperimps.begin(); superimpiter!=allsuperimps.end(); superimpiter++){
                cout << "later" << superimpiter->first << endl;
                vo.print_vector(superimpiter->second);
            }*/

            // at this point, the matches for a query_star are known

            // find GDTTS for these matches by picking the top 100
            // pick the top match and superimpose it against the rest 99 in the top 100
                // cout that GDTTS

            std::string best_match;
            if (allsuperimps.empty()){
                rmsd = penalty;
                bm_rmsd = penalty;
                best_match = "No matches found";
                //cout << "No matches found" << endl;
            }
            else
            {
                std::sort(allsuperimps.begin(), allsuperimps.end());
                best_match= allsuperimps[0].second[star_size];
                std::string best_match_target = allsuperimps[0].second[star_size+1];
                //std::cout << "Superstar is on: \n" << best_match << endl;
                std::cout << "Superstar was: " << best_match_target << endl;
                rmsd = (allsuperimps[0]).first;

                //for (auto j=0; j<101 and allsuperimps[j].first > 0 ; j++ )
                //{
                    //cout << allsuperimps[j].first << ",";
                //}
                //cout << endl;
                // cout the coordinates of permuted atoms
/*                for (int j=0; j<star_size; j++){
                    std::cout << allsuperimps[0].second[j] << endl;
                }
*/
                std::vector<float> rmsdvalues;
                //for (auto j=1; allsuperimps[j].first < 2.0 ; j++){
                for (auto j=1; j<101 and allsuperimps[j].first < gdtrmsdcutoff ; j++)
                {
                    rmsdvalues.push_back(allsuperimps[j].first);
                }
                if (rmsdvalues.empty())
                {
                    std::cerr << "No matches within tolerance for scoring. Exiting now!" << std::endl;
                    std::cout << "SCORE Query: 0" << std::endl;
                    std::exit(EXIT_FAILURE);
                }
                cout << "RMSD list for scoring: "; vo.print_vector(rmsdvalues);
                //cout << "bleh" << endl ;
/*                std::vector<string> targets;
                for (auto j=1; j<11; j++)
                {
                    targets.push_back(allsuperimps[j].second[star_size+1]);
                }
                vo.print_vector(targets);
*/
                cout << "Top " << rmsdvalues.size() << " rmsdvalues used for query score and for best-match scoring" << endl;
                float query_score = score_calculator(rmsdvalues, gdtrmsdcutoff);
                cout << "SCORE Query: " << query_score << endl;
                cout << "--------------------" << endl;

                std::vector<std::string> bm_targetstars;
                for (auto j=1; j<101 and allsuperimps[j].first < gdtrmsdcutoff ; j++)
                {    // note that j init at 1 to skip the best_match_target
                    bm_targetstars.push_back(allsuperimps[j].second[star_size+1]);
                    //cout << allsuperimps[j].first << "," ;
                }
                // vo.print_vector(bm_targetstars);
                //cout << bm_targetstars.size() << " is the number of stars superimposed on the best-match star" << endl;



                // SUPERIMPOSE BEST-MATCH-TARGET AGAINST THE TOP 100 MATCHES

                // Get best_match_target as a query_star with all permutations
                // process query and return two things:
                std::map<string, vector<double> > bm_map_strcoordinates;               //   the map of pairs of 
                                                                                     //     atom-details:coordinates
                std::vector<std::vector<string>> bm_allpermutvecs;        // vector with permutation vectors of the atom-names
                //std::cout << "here" << endl;                            
                std::string bm_query_compo;
                // use query star processor function
                query_processor(best_match_target,star_size, bm_map_strcoordinates, bm_allpermutvecs, bm_query_compo);
                // Superimpose all these permutations to the top 100 after the Superstar 
                std::vector< pair<float, std::vector<string> > > bm_allsuperimps;
                //all superimpositions to be stored as a pair of rmsd and vector of strings of atom-details
                /** loop over each string in targetstars vector
                    for each string in the vector, find coordinates and superimpose on q_vecmap_strcoordinates
                    find rmsd and append rmsd,details to a vector
                */

                for (auto bm_target_star : bm_targetstars)
                {
                    superimpose_stars_2(bm_allpermutvecs, bm_target_star, star_size, 
                                        bm_map_strcoordinates, penalty, sup_cutoff,
                                        bm_allsuperimps);
                }

                /*for (int j=0; j< allsuperimps.size(); j++){
                    std::cout << allsuperimps[j].first <<" : " ;
                        vo.print_vector(allsuperimps[j].second);
                        std::cout << " : "
                        << allsuperimps[j].second.at(star_size) << endl;
                }*/

                std::string bm_best_match;
                if (bm_allsuperimps.empty()){
                    bm_rmsd = penalty;
                    best_match = "No matches found for during best-match scoring";
                    //cout << "No matches found" << endl;
                }
                else
                {
                    std::sort(bm_allsuperimps.begin(), bm_allsuperimps.end());
                    best_match = bm_allsuperimps[0].second[star_size];
                    std::string bm_best_match_target = bm_allsuperimps[0].second[star_size+1];
                    //std::cout << "Superstar is on: \n" << best_match << endl;
                    std::cout << "Superstar was: " << bm_best_match_target << endl;
                    bm_rmsd = (bm_allsuperimps[0]).first;

/*                    for (int j=0; j<star_size; j++){
                        std::cout << bm_allsuperimps[0].second[j] << endl;
                    }
*/
                    std::vector<float> bm_rmsdvalues;
                    for (auto j=0; j< bm_allsuperimps.size(); j++){
                        bm_rmsdvalues.push_back(bm_allsuperimps[j].first);
                    }
                    cout << "RMSD list for scoring: "; vo.print_vector(bm_rmsdvalues);
                    //cout << "Top " << bm_rmsdvalues.size() << " rmsdvalues used for best-match's score" << endl;
                    float bm_score = score_calculator(bm_rmsdvalues, gdtrmsdcutoff);
                    cout << "SCORE Superstar: " << bm_score << endl;
                }
            }

            // take filenames of both and concat them then add .star : this is the outfilename
            /*string output_file = strctre1_file.substr(0, strctre1_file.find_last_of("/")) + "/"
                                    + strctre1_file.substr(strctre1_file.find_last_of("/") + 1, strctre1_file.length())
                                    + "-"
                                    + strctre2_file.substr(strctre2_file.find_last_of("/") + 1,strctre2_file.length())
                                    + ".star";
                                    */
            //display rmsd
            std::cout << "\nTop RMSD of query star is:" << rmsd << endl << "Top RMSD of best-match star is " << bm_rmsd << endl;

            //uncomment below block to print out best_match details


            /*Printing the transformed coordinates, un-comment below line to print*/
            //vo.print_2Dvector(store_str1repatom);

            /* uncomment below code block to print output everytime */
            /*clock_t end = clock();

            cout << "\nElapsefrom ‘std::map<std::__cxx11::basic_string<char>, std::vector<double> >’ to ‘std::__cxx11::string {aka std::__cxx11::basic_string<char>}d Time(seconds):" << setprecision(3) << double(
                    end - start) / CLOCKS_PER_SEC
                    << "\nElapsed Time(milliseconds):" << setprecision(3)
                    << ((double) (end - start) / CLOCKS_PER_SEC) * 1000 << endl;*/
        }
        else{ // if targetstars is empty
            rmsd = penalty;
            std::cout << "\nRMSD is: \n" << rmsd << endl;
            std::cout << "SCORE Query: 0" << std::endl;
            std::cerr << "No target stars in input" << endl;
        }
    } catch (std::exception &e) {

        std::cout << e.what();
        return EXIT_FAILURE;
    }

    return 0;
}


