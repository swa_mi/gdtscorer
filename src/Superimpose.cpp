/*
 * Superimpose.cpp
 *
 * Created on: 2018-10-10
 * 		Author: Swastik
 */

//#include <iostream>
//#include <fstream>
//#include <cstdlib>
//#include <string>
//#include <cstring>
//#include <cstdio>
//#include <iomanip>

#include <boost/algorithm/string.hpp>
//#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/numeric/ublas/io.hpp>

#include "StarParser.h"
#include "jacobi_eigenvalue.h"
#include "AlignInterface.h"
#include "vector_operation.h"


//using namespace std;
//using namespace boost;

inline bool file_exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) 
    {
        fclose(file);
        return true;
    } else {
        return false;
    }   
}

inline int countIfConditionSatisfied (const float &cutoff, const std::vector<float> &rmsdvalues){
	float count = 0;
	for (auto rmsdvalue : rmsdvalues){
		if (rmsdvalue < cutoff){
			count +=1;
		}
	}

	return count;
}


// float function that clusters all-against-all for all the stars given as input
float superimpose_stars(
	std::vector< std::vector<double> > &store_str1repatom, // this will store coordinates for query
    std::vector< std::vector<double> > &store_str2repatom,
	float &penalty,
	float &sup_cutoff
	){

    //vector_operation vo;
    AlignInterface ai;

    double rmsd = penalty;
    double rmsd2 = penalty;
    float so=0.0;
    float so2=0.0;

    // since str1 gets transformed after superimposition,
    // use a copy of str1 and str2 for the second superimposition
    std::vector<std::vector<double> > store_str1repatom_dup(store_str1repatom);
    std::vector<std::vector<double> > store_str2repatom_dup(store_str2repatom);

    //vo.print_2Dvector(store_str1repatom);
    //vo.print_2Dvector(store_str2repatom);

    ai.Align_stars(store_str1repatom, store_str2repatom, rmsd);
    ai.find_so(store_str1repatom, store_str2repatom, so, sup_cutoff);

    ai.Align_stars(store_str2repatom_dup, store_str1repatom_dup, rmsd2);
    ai.find_so(store_str2repatom_dup, store_str1repatom_dup, so2, sup_cutoff);

    float minrmsd = 9001;           // POWER OVER 9000! (But that's a bad thing for this protocol TBH)

    // if either or both of so and so2 are 100, then
    // add the rmsd:temporaryvec1 pair or rmsd2:temporaryvec1 pair respectively to allsuperimps
    // later sort for the best rmsd out of those and output that rmsd and temporaryvec1
    if (so==100 && so2==100 && rmsd<sup_cutoff && rmsd2<sup_cutoff){
    	if (rmsd <= rmsd2){
	    	minrmsd = rmsd;
	        //allsuperimps.push_back(make_pair(minrmsd, temporaryvec1));
    	}
    	else if (rmsd > rmsd2){
    		minrmsd =rmsd2;
    		//allsuperimps.push_back(make_pair(minrmsd, temporaryvec1));
    	}
    }
    else if (so!=100 && so2==100 && rmsd<sup_cutoff){
    	minrmsd =rmsd2;
		//allsuperimps.push_back(make_pair(minrmsd, temporaryvec1));
    }
    else if (so==100 && so2!=100 && rmsd2<sup_cutoff){
    	minrmsd =rmsd;
    }
    else {
    	minrmsd = penalty;    	
    }
    /*if (so2==100){
    	rmsdvec.push_back(rmsd2);
        //cout << "|" << permutarr << "|" << endl;
        //temporaryvec1.push_back(std::to_string(so));
        temporaryvec1.push_back(t_gpdbfilename);
        //cout << rmsd <<" | " << rmsd2 << endl;
        //cout << rmsd << endl;
        //vo.print_2Dvector(temporaryvec1);
        //cout << " so 100 " << t_gpdbfilename << endl;
        allsuperimps.push_back(make_pair(rmsd2, temporaryvec1));
    }
*/
    return minrmsd;
}

// float function that calculates the GDTTS score of top 100 matches given in input as a vector
float score_calculator(
	std::vector<float> rmsdvalues,
    float last_cutoff
	){
	// rmsdvalues is a vector of length 100
	// score = ( f(0.5) + f(1.0) + f(1.5) + f(2.0) )/4
    float final_score = 0.0;
    if (!rmsdvalues.empty())
    {
        //float last_cutoff = 2.0;
        float intervals = 0.1;
        float startintervals = 1.0;
        float NoOfTerms = ((last_cutoff - startintervals) /intervals)+1;
        float total = rmsdvalues.size();
        float fractionCount=0;
        final_score=0.0;
        float termToAdd=0;
        //std::cout << NoOfTerms << " is no of terms " << std::endl;
        for (float i=startintervals; i<=last_cutoff; i= i+intervals){
            fractionCount = countIfConditionSatisfied (i, rmsdvalues)/total;
            cout << "f_" << i << " is: " << fractionCount << "; ";
            termToAdd = fractionCount/NoOfTerms;
            //cout << "term to be added is "<< termToAdd << endl;
            final_score = final_score + termToAdd;
            //cout << final_score << endl;
        }
        cout << endl;
    }
    else
    {
        final_score = 0.0;
    }

	
	return final_score;
}



void superimpose_stars_2(
	std::vector<std::vector<std::string>> &allpermutvecs,
	std::string &target_star,
	int &star_size,
	std::map<string, vector<double> > q_map_strcoordinates, // map of pairs of atomdetails and coordinates
	float &penalty,
	float &sup_cutoff,
    std::vector< std::pair<float, std::vector<std::string> > > &allsuperimps

	)
{
	//Read target_star ATOM statements into a vector
    std::map<int, std::string> t_map_atomstatements;          //  map of line number to atom statements in gpdb file, for whole file
    std::vector<string> t_vec_atomstatements;
    // t_vec_atomstatements is going to be of the form:
    /*
    ["ATOM      7 r1   GLN H   3      38.621  -27.34   1.484        2.71",
    "ATOM      8 r2   GLN H   3      40.562  -25.93   1.889        2.85",
    ...
    "ATOM     20 r3   ARG H   7      39.363 -31.798   4.613        3.49"]
    */

	std::vector<string> t_starvector;
    std::string t_gpdbfilename;
    // START LOOPING OVER TARGETS

    std::vector<vector<double> > store_str2repatom; // this will store coordinates for target inside the for loop (see below)
    std::vector<vector<double> > store_str1repatom; // this will store coordinates for query inside the for loop (see below)


	// target_star is of type std::string
    boost::split(t_starvector, target_star, boost::is_any_of(" \t"), boost::token_compress_on);
    //starvector now looks like [ ./2mta     2021    2020    2019    2010    2005    2009    2004    2003]
    t_gpdbfilename = t_starvector.front() + ".gpdb";
    if ( !file_exists(t_gpdbfilename) ){
        throw std::runtime_error("ERROR: target gpdb file not found with path " + t_gpdbfilename + "\n\n");
    }

    std::ifstream t_gpdbif(t_gpdbfilename, std::ifstream::in);
    if (t_gpdbif.is_open()){
        Scan_star_for_stars(t_starvector, t_vec_atomstatements, t_map_atomstatements, t_gpdbif);
        //vo.print_vector(t_vec_atomstatements);
    } // end if block checking for open t_gpdbif filestream object
    //q_rep_atom == t_rep_atom?

    // Read in coordinates and other variables of target vector
    std::vector<pair<string, vector<double>> > t_vecmap_strcoordinates;    //  atomdetails:coordinates pairs
    std::map<string, vector<double> > t_map_strcoordinates;               //the map of --do--
    
    Scan_ATOM_for_coord(t_vec_atomstatements,          // vector with ATOM statements for members of star
                        t_vecmap_strcoordinates,
                        t_map_strcoordinates);     //  atomdetails: coordinates pairs

    //check how t_vecmap_strcoordinates looks like:                    
    /*for (auto iter1 : t_vecmap_strcoordinates){
        cout << iter1.first;
        vo.print_vector(iter1.second);
    }*/

    //superimpose and find all rmsds for all permutations in all target_pdbs
    std::string compoarr2;       //to store composition of chm-groups/atom-names for later comparison
    std::string da_atomname;     //temp atom-name string var

    //place all the atom details (basically columns 12:26)
    //from the keys in vecmap_str1coordinates in
    //keys1 which is an array of size= star_size
    for (auto targetcoordpair : t_vecmap_strcoordinates){
        store_str2repatom.push_back(targetcoordpair.second);
        da_atomname = targetcoordpair.first.substr(6,4);
        boost::trim(da_atomname);
        compoarr2 += da_atomname;
        //cout << targetcoordpair.first << ":" ;
        //vo.print_vector(targetcoordpair.second);
    }

    std::string permutarr;       //temp string for the composition of strctre-1 in one of the permutations
    std::string atom_name;       //temp string for atom_name to be added to permutarr
    std::vector<double> v;  //stores coordinates of strctre-1 in each permutation
    std::vector<std::string> temporaryvec1;
    std::string atomids="";
    std::string tempatomid="";

    std::vector< std::pair<float, std::vector<string> > > temp_allsuperimps;

    for (unsigned int i=0; i < allpermutvecs.size(); i++){

        for (int j=0; j<star_size; j++){
            atom_name=allpermutvecs[i][j];
            tempatomid = atom_name.substr(0,5);
            boost::trim(tempatomid);
            atomids += tempatomid;
            atom_name=atom_name.substr(6,4);
            boost::trim(atom_name);
            permutarr += atom_name;
            tempatomid.clear();
        }
        //cout << "|" << permutarr << "|" << compoarr2 << endl;

        if (permutarr==compoarr2){
            //cout << "yay " <<  t_gpdbfilename <<" | " <<permutarr << endl;
            for (int k=0; k<star_size; k++){
                v= q_map_strcoordinates[allpermutvecs[i][k]];
                store_str1repatom.push_back(v);
                temporaryvec1.push_back(allpermutvecs[i][k]);
            }
            // superimpose str2 and str1, and str1 on str2
            // take the best of the two superimpositions, since A on B != B on A
            float minrmsd = superimpose_stars(store_str1repatom,store_str2repatom,
                                            penalty, sup_cutoff);

            /*else {
                cout << "so not 100pc " << endl;
                cout << t_gpdbfilename << endl;

            }*/
            if (minrmsd < 9000)
            {
                temporaryvec1.push_back(t_gpdbfilename);
                temporaryvec1.push_back(target_star);
                temp_allsuperimps.push_back(make_pair(minrmsd, temporaryvec1));
            }
            temporaryvec1.clear();
        }
        store_str1repatom.clear();
        permutarr.clear();
        atomids.clear();
        v.clear();

    } //end for loop for superimposition against all permutations

    if (!temp_allsuperimps.empty())
    {
        std::sort(temp_allsuperimps.begin(), temp_allsuperimps.end());
        /*for (auto temp_allsuperimp : temp_allsuperimps)
        {
        	cout << temp_allsuperimp.first << ", ";
        }
    	cout << endl;*/
        allsuperimps.push_back(temp_allsuperimps[0]);
        /*for (auto temp_superimp : temp_allsuperimps)
        {
            allsuperimps.push_back(temp_superimp);
        }*/
    }
    /*else
    {
        cout << "No matches found for target_star " << target_star << endl;
    }*/

    t_vecmap_strcoordinates.clear();
    t_map_strcoordinates.clear();
    t_map_atomstatements.clear();
    t_vec_atomstatements.clear();
    store_str2repatom.clear();
    compoarr2.clear();
    target_star.clear();
    t_starvector.clear();
    temp_allsuperimps.clear();


}


// void function that sorts all stars given in input into the clusters given in terms of their representatives (in input)
// void superimposer function that superimposes A->B and B->A and returns the rmsd and so of the superimposition
// void 

/*

*/
