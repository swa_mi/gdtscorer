/*
 * Inputprocessor.cpp
 *
 * Created on: 2018-10-26
 *      Author: Swastik
 */

#include <boost/algorithm/string.hpp>

#include "StarParser.h"
#include "vector_operation.h"


inline bool file_exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) 
    {
        fclose(file);
        return true;
    } else {
        return false;
    }   
}

// process query star
    // returns updated permutedvec and map of coordinates for query star
void query_processor(
    std::string &query_star,       //query star of the form 101\t102\t103...
    int &star_size,
    std::map<std::string, std::vector<double> > &q_map_strcoordinates,               //   the map of pairs of 
                                                                         //     atom-details:coordinates
    std::vector<std::vector<std::string>> &allpermutvecs,
    std::string &query_composition                          // query star compo in terms of cg_names
   )
{
    vector_operation vo;

    std::map<int, std::string> q_map_atomstatements;
    std::vector<std::string> q_vec_atomstatements;
    // q_vec_atomstatements is going to be of the form:
    /*
    ["ATOM      7 r1   GLN H   3      38.621  -27.34   1.484        2.71",
    "ATOM      8 r2   GLN H   3      40.562  -25.93   1.889        2.85",
    ...
    "ATOM     20 r3   ARG H   7      39.363 -31.798   4.613        3.49"]
    */
    std::vector<std::string> q_starvector;
        
        
    boost::split(q_starvector, query_star, boost::is_any_of(" \t"), boost::token_compress_on);
    //starvector now looks like [ ./2mta     2021    2020    2019    2010    2005    2009    2004    2003]
    std::string q_gpdbfilename = q_starvector.front() + ".gpdb";

    std::cerr << "Query star no. is " << q_starvector[1] << " in " << q_gpdbfilename << std::endl;
    //vo.print_vector(q_starvector);
    
    if ( !file_exists(q_gpdbfilename) ){
        throw std::runtime_error("ERROR: query gpdb file not found with path " + q_gpdbfilename + "\n\n");
    }

    std::ifstream q_gpdbif(q_gpdbfilename, std::ifstream::in);
    if (q_gpdbif.is_open()){
        Scan_star_for_stars(q_starvector, q_vec_atomstatements, q_map_atomstatements, q_gpdbif);
        //vo.print_vector(q_vec_atomstatements);
    } // end if block checking for open q_gpdbif filestream object
    //vo.print_vector(q_starvector);
    //q_rep_atom

    // Read in coordinates and other variables of query vector
    std::vector<std::pair<std::string, std::vector<double>> > q_vecmap_strcoordinates;    //  atomdetails:coordinates pairs
    
    Scan_ATOM_for_coord(q_vec_atomstatements,          // vector with ATOM statements for members of star
                        q_vecmap_strcoordinates,
                        q_map_strcoordinates);     //  atomdetails: coordinates pairs

    //check how q_vecmap_strcoordinates looks like:                    
    //for (auto iter1 : q_vecmap_strcoordinates){
        //cout << iter1.first;
        //vo.print_vector(iter1.second);
    //}
    /*q_vecmap_strcoordinates = 
    dict{
         7 r1   GLN H   3  : 38.621, -27.34, 1.484
         8 r2   GLN H   3  : 40.562, -25.93, 1.889
        11 r1   ALA H   4  : 35.974, -28.632, 2.453
        12 r8   ALA H   4  : 36.566, -27.78, 4.584
        17 r1   ARG H   7  : 36.603, -32.429, -0.936
        18 r2   ARG H   7  : 38.866, -31.969, -0.025
        19 r2   ARG H   7  : 38.541, -31.49, 1.388
        20 r3   ARG H   7  : 39.363, -31.798, 4.613
    }

    */

    std::vector<std::pair<std::string, std::vector<double>> >::iterator queryiterator;    //to iterate over all entries of vector
    std::string keys1[star_size];                       //to store all keys in the pair entries of the vector
    std::string keys2[star_size];                       //to store all keys in the pair entries of the vector
    std::string temp_aname;
    int it;                                         //to store index of iterator
    for (queryiterator=q_vecmap_strcoordinates.begin(); queryiterator!= q_vecmap_strcoordinates.end(); queryiterator++) {
        it=std::distance(q_vecmap_strcoordinates.begin(), queryiterator);
        temp_aname = queryiterator->first.substr(6,4);
        boost::trim(temp_aname);
        keys1[it]=queryiterator->first;
        keys2[it]=temp_aname;
        temp_aname.clear();
        

    }
   
    std::sort(keys1+1, keys1+star_size);  //sort to reach lexicographically lowest permutation
    std::sort(keys2+1, keys2+star_size);  //sort to reach lexicographically lowest permutation
    int it2;
    query_composition=keys2[0];
    //std::cout << query_composition << std::endl;
    //boost::trim(query_composition);
    //std::cout << query_composition << std::endl;
    for (auto keys2_iter=keys2+1; keys2_iter!=(keys2+star_size); keys2_iter++)
    {
        try
        {
            it2=std::distance(keys2, keys2_iter);
            temp_aname=keys2[it2];
            //std::cout << temp_aname << std::endl;
            //std::cout << temp_aname << std::endl;
            //boost::trim(temp_aname);
            //std::cout << temp_aname << std::endl;
            if (temp_aname != "")
            {
                query_composition = query_composition + "_"+temp_aname;            
            }
        }
        catch (std::bad_alloc& ba)
        {
            std::cerr << "bad_alloc caught: " << ba.what() << std::endl;
        }
        //std::cout << "updated: " << query_composition << std::endl;

    }
    //std::cout << "final: " << query_composition << std::endl;
    
    std::vector<std::string> permutedvec;

    do {
            for (int i=0; i<star_size; i++){
                permutedvec.push_back(keys1[i]);
            }
            allpermutvecs.push_back(permutedvec);
            permutedvec.clear();
        } while( std::next_permutation( (keys1+1), keys1+star_size));
    /**allpermutvecs now has vectors such that
    each vector is a permutation of the atoms in query star
    however only points except the first point(central point) are permuted, so # of permutations is greatly reduced
    permute only last n-1 atoms/chm-groups
    */

}

void target_processor(
    std::string &query_composition,              // query composition, underscore delimited
    std::string &cliqs_dir,                     // target_dir with the .cliqs files
    int &sample_size,                            
    std::vector<std::string> &targetstars,
    std::string &target_dir,
    std::string &query_gpdbid,
    std::string &reject_pdbid
    )
{
    //uncomment this line only when debugging, when vector_operation is actually required
    //vector_operation vo;

    /*do what?
        - read all the lines from target_dir/query_compo.cliqs
        - shuf all the lines
        - pick up the first sample_size number of lines
        - return them as a vector of strings, each string being a line
    */

    //rejeect_pdbid is a comma-separated list of pdbIDs but is a single string. split it into a vector of strings
    std::vector<std::string> rejectPDBsID;
    boost::split(rejectPDBsID, reject_pdbid, boost::is_any_of(","), boost::token_compress_on);
    //vo.print_vector(rejectPDBsID);

    std::vector<std::string> targetstars_temp;

    std::string cliqsfile = cliqs_dir + "/" + query_composition + ".cliqs";

    std::ifstream inputstarsfile(cliqsfile);
    std::string line;
    int linecount = 0;
    if( inputstarsfile.is_open() )
    {
        while( getline(inputstarsfile, line) )
        {
            //cout << query_gpdbid << endl;
            // need to check only against first 4 characters of the target cliq line whether it matches with any of the ones to be rejected
            std::string subline = line.substr(0, 4);
            
            if ( line.find(query_gpdbid) != 0 and std::find(rejectPDBsID.begin(), rejectPDBsID.end(), subline) == rejectPDBsID.end() )
            {
                targetstars_temp.push_back(target_dir + "/" + line);
                linecount = linecount+1;
            }
            else
            {
                std::cout << "Rejecting star: " << line << std::endl;
            }            
        } // end while block looping over lines in .star filestream object
    } // end if block checking if file is open
    //vo.print_vector(targetstars_temp);
    //std::cout << targetstars_temp.size() << std::endl;
    
    // use current time in seconds, as the seed for the random num generation
    srand(time(0));
    // using that seed, shuffle all lines in targetstars_temp
    std::random_shuffle(targetstars_temp.begin(), targetstars_temp.end());

    // extract the first sample_size number of lines from targetstars_temp and add it to targetstars
    std::vector<string>::const_iterator first = targetstars_temp.begin();
    std::vector<string>::const_iterator last;
    if ( sample_size <= targetstars_temp.size()  )
    {
        last = targetstars_temp.begin() + sample_size;
    }
    else
    {
        last = targetstars_temp.end();
    }
    std::vector<string> targetstars_temp2(first, last);
    targetstars = targetstars_temp2;
    //vo.print_vector(targetstars);

}
