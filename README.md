Superimposition code for cliques
Allowed options:
  -h [ --help ]               produce help message
  -i [ --input-cliqfile ] arg path to .cliq file
  -q [ --query-dir ] arg      Path to dir where query should be extracted from
  -t [ --target-dir ] arg     Path to dir where targets should be extracted 
                              from
  -n [ --size ] arg (=8)      Set size of clique
  -r [ --rmsd ] arg (=2)      Set rmsd cutoff for succesful 
                              superimposition/match
  -p [ --penalty ] arg (=10)  Set penalty for non-superimposition

